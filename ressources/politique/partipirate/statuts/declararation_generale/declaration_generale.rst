
.. _statuts_pirate_generale:

=====================================
Déclaration générale
=====================================

.. seealso::

   - https://wiki.partipirate.org/Statuts

.. contents::
   :depth: 3


Article 1
==========

Nous, pirates, formons le Parti Pirate dans le respect de la loi du 1er
juillet 1901 et le décret du 16 août 1901 pour une durée illimitée.

Article 2
==========

Nous, pirates, militons dans le respect du Code des Pirates

I - Les Pirates sont libres
------------------------------

Nous, Pirates, chérissons la liberté, l'indépendance, l'autonomie et
refusons toute forme d’obédience aveugle.

Nous affirmons le droit à nous informer nous-mêmes et à choisir notre
propre destin.

Nous assumons la responsabilité qu’induit la liberté.

II - Les Pirates respectent la vie privée.
---------------------------------------------

Nous, Pirates, protégeons la vie privée.

Nous combattons l’obsession croissante de surveillance car elle empêche
le libre développement de l’individu.

Une société libre et démocratique est impossible sans un espace de
liberté hors-surveillance.

III - Les Pirates ont l’esprit critique
-----------------------------------------

Nous, Pirates, encourageons la créativité et la curiosité.

Nous ne nous satisfaisons pas du statu quo.

Nous défions les systèmes, traquons les failles et les corrigeons.

Nous apprenons de nos erreurs.

IV - Les Pirates sont environnementalistes
----------------------------------------------

Nous, Pirates, luttons contre la destruction de l'environnement et toute
forme de capitalisation des ressources.

Nous militons pour la pérennité de la nature et de ce qui la compose.

Nous n’acceptons aucun brevet sur le vivant.

V - Les Pirates sont avides de connaissance.
---------------------------------------------

L’accès à l’information, à l’éducation et au savoir doit être illimité.

Nous, Pirates, soutenons la culture libre et le logiciel libre.

VI - Les Pirates sont solidaires.
------------------------------------

Nous, Pirates, respectons la dignité humaine et rejetons la peine de mort.

Nous nous engageons pour une société solidaire défendant une conception
de la politique faite d’objectivité et d’équité.

VII - Les Pirates sont cosmopolites
------------------------------------

Nous, Pirates, faisons partie d’un mouvement mondial.

Nous nous appuyons sur l'opportunité qu’offre Internet de penser et
d'agir par-delà les frontières.

VIII - Les Pirates sont équitables
-------------------------------------

Nous, Pirates, luttons pour l'égalité entre les personnes, sans considération
de genre, de couleur de peau, d'âge, d'orientation sexuelle, de niveau
d’études, de statut, d'origine ou de handicap.

Nous militons pour la liberté de s’épanouir.

IX - Les Pirates rassemblent
------------------------------

Nous, Pirates, ne prétendons pas avoir la solution à tous les problèmes.

Nous pensons que réfléchir collectivement est nécessaire, nous invitons
donc tout le monde à s'engager politiquement, à contribuer à partir
de ses connaissances, expériences et perspectives.

Nous saluons les contributions qui sortent des sentiers battus.

X - Les Pirates relient
-------------------------

Nous, Pirates constatons que tant les bonheurs individuels que communs
se fondent sur les liens que nous tissons avec nous mêmes, les autres,
la société, la nature et le monde.

Par la numérisation et Internet, la moitié de la population mondiale est
connectée en un réseau horizontal et décentralisé.

Cette conscience collective transforme le monde.

XI - Les Pirates font confiance
----------------------------------

Nous, Pirates avons confiance en nous et osons faire confiance aux autres.

Nous croyons en la collaboration et contribuons aux communs ainsi qu'aux
projets collectifs.

Nous portons un regard bienveillant sur la vie en communauté.

XII - Les Pirates font preuve d'audace
-----------------------------------------

Nous, Pirates, n'attendons pas que des solutions viennent à nous mais
nous organisons par nous-mêmes pour répondre aux problèmes que nous
rencontrons.

**Nous croyons en la force des mouvements collaboratifs et horizontaux**.

XIII - Les Pirates sont hétéroclites
----------------------------------------

Nous, Pirates, voulons apprendre de nos différences pour progresser et
dépasser la notion de polarité.

Nous ne croyons pas au traditionnel bipartisme.

Tout le monde peut être Pirate.
