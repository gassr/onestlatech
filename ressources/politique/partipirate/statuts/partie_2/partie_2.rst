
.. _statuts_pirate_partie_2:

========================================
Partie 2 : du fonctionnement ordinaire
========================================

.. seealso::

   - https://wiki.partipirate.org/Statuts

.. toctree::
   :maxdepth: 3

   titre_1/titre_1
