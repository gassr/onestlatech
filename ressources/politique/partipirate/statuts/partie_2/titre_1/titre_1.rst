
.. _statuts_partie_2_titre_1:

========================================
Titre 1 Des assemblées des Pirates
========================================

.. seealso::

   - https://wiki.partipirate.org/Statuts


.. contents::
   :depth: 3

Chapitre 1 : De l'Assemblée Permanente
========================================

Article 4
------------

L'Assemblée Permanente se tient dans la régularité prévue au Règlement intérieur.

Article 4-1
---------------

L’assemblée permanente est le regroupement de tous les pirates.

Elle est compétente pour approuver :

1. La création des équipages et de leur reconduction à l’échéance de la
   période d’activité ;
2. Les rapports de fonctionnement des équipages ;
3. Les budgets des projets annuels ou ponctuels ;
4. Les Codes de fonctionnement des conseils, équipes, coordinations
   et équipages ;
5. Les nominations faites par cooptation ;
6. Les motions programmatiques ;
7. Les motions politiques ;
8. Les investitures et les soutiens de candidats aux élections ;
9. Les motions de modification du règlement intérieur ;
10. L’ordre du jour de l’assemblée statutaire ;
11. Les sanctions prises par le Tribunal des Pirates ;
12. Toutes décisions ne relevant pas de la compétence d’un autre organe interne.

Article 4-2
------------

Les modalités d'organisation, de procédure et de vote sont précisées au
Règlement intérieur.

Article 4-3
-------------

L'Assemblée Permanente est convoquée annuellement par la publication du
calendrier des sessions.

Article 4-4
-------------

L'ordre du jour est établi par le secrétariat en relation avec les Conseils,
les équipages et les pirates.


Chapitre 2 : De l'Assemblée Statutaire
===========================================

Article 5
------------

L'Assemblée Statutaire se tient une fois par an.

Elle est la dernière assemblée du calendrier des sessions.

Article 5-1
-------------

L'Assemblée Statutaire est compétente pour :

- Approuver le rapport d'activités et financier du secrétariat
- Modifier les statuts
- Élire les membres des Conseils
- Voter le calendrier des sessions des Assemblées Statutaire et Permanente

Article 5-2
--------------

Les modalités, d'organisation, de procédure et de vote sont précisées
au Règlement intérieur.

Article 5-3
------------

L'Assemblée Statutaire est convoquée par la publication du calendrier des sessions.

Article 5-4
-------------

L'ordre du jour est établit par l'Assemblée Permanente.

Article 5-5
------------

Si nécessaire, l'Assemblée Permanente peut transformer une de ses sessions
en Assemblée Statutaire en modifiant le calendrier des sessions.
