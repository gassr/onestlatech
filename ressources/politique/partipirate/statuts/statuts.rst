
.. _statuts_partipirate:

=====================================
Statuts Parti pirate
=====================================

.. seealso::

   - https://wiki.partipirate.org/Statuts

.. toctree::
   :maxdepth: 3

   declararation_generale/declaration_generale
   partie_1/partie_1
   partie_2/partie_2
