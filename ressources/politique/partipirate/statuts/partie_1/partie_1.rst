
.. _statuts_pirate_partie_1:

=====================================
Partie 1 : de la qualité de pirate
=====================================

.. seealso::

   - https://wiki.partipirate.org/Statuts#PARTIE_1_:_DE_LA_QUALIT%C3%89_DE_PIRATE

.. contents::
   :depth: 3

Article 3
=============

Est membre du Parti Pirate, toute personne physique ayant réglé sa cotisation.

Le montant de la cotisation est précisé au Règlement intérieur.

Toute cotisation versée ne sera pas remboursée sauf à ce que le Secrétariat
refuse l'adhésion.

Dans ce cas, la remboursement se fait selon les modalités prévues au
Règlement intérieur.

Article 3-1
===============

La qualité de membre se perd :

- Par décès
- Par démission
- Par non paiement de la cotisation
- Par exclusion, après décision de l'Assemblée Permanente

Article 3-2
=============

Peuvent adhérer au Parti Pirate les personnes qui adhèrent à un autre
parti politique.

Ils doivent le déclarer au moment de leur adhésion ou de leur ré-adhésion.

Le secrétariat peut s'opposer à l'adhésion si l'adhésion à un autre
parti politique est contraire aux valeurs du Parti Pirate.
