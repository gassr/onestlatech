.. index::
   ! adullact

.. _adullact:

============================================================================================================
adullact
============================================================================================================

.. seealso::

   - https://adullact.org/


.. contents::
   :depth: 3


Description
==============================

Fondée en 2002, l'ADULLACT a pour objectifs de soutenir et coordonner
l'action des Administrations et Collectivités territoriales dans le but
de promouvoir, développer et maintenir un patrimoine de logiciels
libres utiles aux missions de service public.

L’ADULLACT, structure unique en Europe, est une initiative née de la
nécessité de voir apparaître une alternative au système des licences
propriétaires, en particulier dans le domaine des logiciels métiers.

En mettant en place des projets informatiques libres répondant aux
besoins précis de ses adhérents et en coordonnant les compétences
territoriales, l’ADULLACT souhaite donner un sens concret à l’idée
de mutualisation des ressources.

Avec une conviction et un principe simples : l'argent public ne doit
payer qu'une fois !

