.. index::
   ! CHATONS

.. _chatons:

============================================================================================================
**CHATONS** est le Collectif des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires
============================================================================================================

.. seealso::

   - https://chatons.org/


.. contents::
   :depth: 3


Description
==============================

Ce collectif vise à rassembler des structures proposant des services en
ligne libres, éthiques et décentralisés afin de permettre aux utilisateur⋅ices
de trouver rapidement des alternatives respectueuses de leurs données
et de leur vie privée aux services proposés par les GAFAM (Google, Apple,
Facebook, Amazon, Microsoft). CHATONS est un collectif initié par
l'association Framasoft en 2016 suite au succès de sa campagne
Dégooglisons Internet.



