.. index::
   pair: Manifeste ; mouton-numerique

.. _manifeste_mouton:

============================================================================================================
Le manifeste du mouton
============================================================================================================

.. contents::
   :depth: 3


Introduction
============

Le collectif du Mouton Numérique s’est constitué autour de la croyance
partagée que, au-delà de sa myriade de 0 et 1 et de ses effets de buzz,
le numérique est ce qu’on appelle en sciences humaines un **fait social total**.

Pour le dire autrement : aucun domaine de la société n’échappe au numérique.
Que ce soit dans nos rapports sociaux, économiques ou politiques, mais
aussi dans la sphère juridique ou de l’intime, le numérique s’immisce
toujours plus insidieusement dans la vie de chacun, même de ceux qui en
sont exclus.

Bien que les pratiques, les savoirs et les accès diffèrent d’une personne
à l’autre, nous nous positionnons tous, et au quotidien, par rapport à lui,
consciemment ou non.

Le numérique a modifié notre rapport au temps, accentuant, de fait,
les phénomènes de rationalisation et d’individualisation de la Modernité
florissante, faisant même renaître pour certains la flamme d’un sacré
qui semblait avoir disparu de notre société désormais sécularisée.

Si le numérique structure de bien des manières la société dans laquelle
nous vivons et tend à participer aux fondations de celle de demain,
le Mouton Numérique veut en connaître les tenants et les aboutissants.

**Nous croyons à l’apport des sciences humaines et de leur regard critique**
pour en saisir les enjeux et les bouleversements et pouvoir proposer des
réponses citoyennes et viables à long terme.

Nous croyons que c’est par le dialogue et la rencontre des différents
acteurs qui font et pensent la société que nous réussirons à garder la
main sur ces mutations et à produire de nouveaux imaginaires capables
d’éclairer cette société qui innove.


Les cinq pattes du Mouton Numérique
=======================================

1/ Un Mouton vaut mieux que deux tu l'auras
----------------------------------------------

Pourquoi attendre demain une présupposée prise de conscience généralisée,
quand on peut dès aujourd’hui travailler à son élaboration ?
C’est sur cette croyance que le Mouton a décidé de sortir de son
enclos aujourd’hui et non demain, car il n’y a pas de grand changement,
sans quelques valeurs sûres et petites actions préalables.

2/ L'herbe n'est pas toujours moins verte ici qu'ailleurs : c'est au Mouton qu'il revient de verdir son pré.
--------------------------------------------------------------------------------------------------------------

Il est toujours plus aisé de regarder et d’envier la situation d’autrui
sans pourtant en connaître toutes les caractéristiques.

Loin de s’apitoyer de son sort, le Mouton croit dur comme fer que
certaines choses peuvent être changées si elles sont examinées et
circonscrites avec soin.

C’est en portant un regard critique sur notre propre herbe et celle de
son voisin que nous pourrons apporter de réelles réponses constructives
aux problèmes que pose le numérique

3/ Quand le numérique rêve de Moutons androïdes, les Moutons se forgent **un humanisme numérique**.
----------------------------------------------------------------------------------------------------

S’il peut arriver aux Androïdes de rêver de Moutons électriques, nous
soupçonnons les magnats du numérique de rêver d’un monde hybride, où
l’homme ne ferait qu’un avec sa machine, **au point d’en oublier son existence**.

Contre cela, nous espérons, grâce à nos débats, conserver un certain
recul sur la situation et proposer de nouveaux imaginaires pouvant
répondre durablement aux enjeux de notre société du presque-tout-numérique.


4/ Une science sans Mouton n'est que ruine du troupeau
----------------------------------------------------------

**Nous pensons qu’il faut éviter toute sacralisation de la technique et
des avancées de la science**.

Chaque innovation apparaît dans un environnement précis qui la modèle.

C’est en interrogeant ces différentes modalités dans nos débats, que
nous comptons prendre un recul suffisant pour recontextualiser les
différents outils numériques et sortir de tout aveuglement de masse.



5/ C'est à coups de moutoneries que l'on devient Mouton Alors, moutonons
---------------------------------------------------------------------------

Apporter des solutions pratiques et concrètes aux problèmes soulevés par
le numérique n’est pas une faculté qui tombe du ciel.

C’est à force d’exercer son regard critique tout en restant à l’écoute
de ce qui se fait et se pense dans la société que nous espérons forger
les armes nécessaires à la société demain.

Alors, n’hésitez plus, participez-vous aussi à nos débats et joignez
votre voix aux Moutons !



