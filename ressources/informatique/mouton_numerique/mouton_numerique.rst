.. index::
   ! mouton-numerique

.. _mouton_numerique:

============================================================================================================
mouton-numerique
============================================================================================================

.. seealso::

   - https://mouton-numerique.org/
   - https://twitter.com/MoutonNumerique


.. contents::
   :depth: 3


Description
==============================

Il fallait cet étrange cocktail pour lancer un Mouton Numérique : une
équipe fondatrice animée par les sciences humaines, fascinée par les
(r)évolutions technologiques et avide de saisir ce qui fait l’essence
de ce monde qui innove.

En commun un objectif : éclairer les choix du futurs avec les armes
du dialogue.


Manifeste
===========

.. toctree::
   :maxdepth: 3

   manifeste/manifeste



