.. index::
   pair: Ressources; Energie

.. _ressources_energie_latech:

=====================================
Ressources énergie
=====================================

.. toctree::
   :maxdepth: 3


   theshiftproject/theshiftproject_energie
   centrale_energie/centrale_energie


