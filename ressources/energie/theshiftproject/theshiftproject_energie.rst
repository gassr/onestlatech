.. index::
   pair: theshiftproject ; Energie

.. _theshiftproject_energie:

============================================================================================================
theshiftproject (énergie)
============================================================================================================

.. seealso::

   - https://theshiftproject.org/
   - https://twitter.com/JMJancovici
   - https://twitter.com/Les_shifters
   - https://twitter.com/theShiftPR0JECT


.. contents::
   :depth: 3


Description
==============================

Le Shift est géré au quotidien par une équipe salariée d’une dizaine de
personnes dont Matthieu Auzanneau est le directeur.

Le président Jean-Marc Jancovici et le Bureau supervisent les activités
tandis que des chefs de projet pilotent les groupes de travail.

Le think tank bénéficie du soutien de bénévoles qui ont constitué une
association autonome : Les Shifters.


