.. index::
   pair: Peter Kalmus; Climat

.. _peter_kalmus:

=====================================
Peter Kalmus
=====================================

.. seealso::

   - https://twitter.com/ClimateHuman
   - https://peterkalmus.net/books/read-by-chapter-being-the-change/


Bio
=====

Climate scientist terrified by what I see.
Revoking fossil fuel's social license.

Founder @ClimateAd

- Co-Founder @EarthHeroOrg
-  #NotMeUs! Views from me not NASA
