.. index::
   pair: sobriété numérique; negaoctet

.. _negaoctet:

=====================================
negaoctet
=====================================

.. seealso::

   - https://negaoctet.org


.. contents::
   :depth: 3

Description
=============

NegaOctet est un projet de recherche qui a pour but le développement
et l’expérimentation d’un référentiel d’évaluation des impacts
environnementaux des services numériques basé sur une approche d’analyse
du cycle de vie (ACV) en vue de leur écoconception.

