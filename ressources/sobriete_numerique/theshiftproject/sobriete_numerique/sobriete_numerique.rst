.. index::
   pair: Sobriété numérique ; theshiftproject
   ! double contrainte carbone

.. _theshiftproject_sobriete:

=========================================================
Sobriété numérique pour les décideurs
=========================================================

.. seealso::

  - https://theshiftproject.us2.list-manage.com/track/click?u=cbe9be2fac311a9f76c543bbd&id=5ebb0fd5e9&e=abed6f0196

.. contents::
 :depth: 3


Rapport
==========

:source: https://theshiftproject.us2.list-manage.com/track/click?u=cbe9be2fac311a9f76c543bbd&id=5ebb0fd5e9&e=abed6f0196

.. figure:: ressources.png
  :align: center


:download:`Télécharger le rapport Deployer-la-sobriete-numerique_Resume_ShiftProject au format PDF <Deployer-la-sobriete-numerique_Resume_ShiftProject.pdf>`


Contexte : le numérique,une transition à repenser
====================================================

Relever les défis du XXIe siècle implique de comprendre les limites
physiques auxquelles sont soumis nos systèmes.

Le **réchauffement climatique**, et le **tarissement progressif de notre
approvisionnement en énergies fossiles** constituent ensemble
la **double contrainte carbone**

Aujourd’hui, **la croissance de nos systèmes numériques est insoutenable**
, +9% d’énergie consommée par an, et est construite autour de modèles
économiques qui rentabilisent l’augmentation des volumes de contenus
consommés et de terminaux et infrastructures déployés, notamment à
travers l’**économie de l’attention**.

La sobriété numérique, c’est passer d’un numérique instinctif voire
compulsif à un numérique piloté, qui sait choisir ses directions : au vu
des opportunités, mais également au vu des risques.


Conclusions principales
=======================

- L’évaluation de la pertinence environnementale doit être systématique.

- Certaines innovations connectées recèlent un potentiel de gain environnemental
  et d’autres n’en ont structurellement pas la capacité : il n’est donc
  justifié ni d’avoir une attitude de rejet généralisé ni de faire montre
  d’une foi aveugle à leur égard.

- Construire un système résilient, c’est être en mesure d’identifier les
  conditions dans lesquelles il est pertinent de déployer une solution
  numérique.
  Ces conditions, propres à chaque situation, doivent être déterminées
  sur la base de bilans pré-visionnels environnementaux des projets dits
  « smart » sans se reposer sur leur simple dénomination

- L’impact de la consommation énergétique (énergie grise et en fonctionnement)
  de la couche numérique appliquée à un système peut surpasser l’économie
  d’énergie venant du gain d’efficacité énergétique du système.

- Le bilan énergétique net n’est souvent positif que si les comportements
  en phase d’utilisation sont orientés par une gouvernance alignée sur
  les objectifs d’économie d’énergie.
