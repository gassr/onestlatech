.. index::
 ! PTEF
 ! Plan de transformation de l’économie française

.. _ptef:

=========================================================
Plan de transformation de l’économie française (PTEF)
=========================================================

.. seealso::

 - https://theshiftproject.org/crises-climat%E2%80%89-plan-de-transformation-de-leconomie-francaise/
 - https://theshiftproject.org/wp-content/uploads/2020/10/201016-Rapport-de-Synthese-Vision-globalev1-PTEF.pdf

.. contents::
   :depth: 3


Vision globale v0
===================

.. seealso::

 - https://theshiftproject.org/wp-content/uploads/2020/07/Rapport-davanvement_Vision-globale_V0_PTEF_Shift-Project.pdf

Vision globale v1 (octobre 2020)
==================================

L’État d’avancement du PTEF comporte une introduction.

Il est segmenté selon quatre logiques:

- secteurs «usages» (mobilité quotidienne, mobilité longue distance,
  logement, usages numériques) ;
- secteurs «services» (santé,culture,défense et sécurité intérieure,
  enseignement supérieur et recherche,administration publique) ;
- secteurs «amont» (agriculture-alimentation,forêt-bois,énergie,fret,
  matériaux et industrie dont ciment-chimie-batteries,industrie automobile) ;
- chantiers transversaux (emploi,finance,résilience et impacts,villes et territoires)


Nous voulons nous assurer que les transformations proposées dans
chaque secteur forment une vision globalement cohérente en termes de
flux physiques: d’une part que l’énergie consommée est bien égale
annuellement à l’énergie produite en amont, et d’autre part
que les quantités de matériaux nécessaires à cette économie sont
connues, chiffrées (pour quelques exemples pour l’instant),
et **compatibles avec les ressources connues de la croûte terrestre**.

Synthèse usage du numérique
=============================

Le secteur aujourd’hui
------------------------

Le secteur du numérique est constitué d’une  composante matérielle
(le système technique, réseaux et fabrication des appareils) et des
usages qu’il génère (échanges de mail, vidéos, usages des smartphones,
stockages des données...).

Au niveau mondial ce secteur représente 3% de la consommation d’énergie
et émet 4% des gaz à effet de serre (GES).

Le secteur du numérique représente 600000 emplois en France.

Les outils de la transformation
-----------------------------------

Plusieurs axes doivent être mobilisés pour stabiliser l’empreinte
écologique du secteur numérique (actuellement +9% par an au niveau mondial).

Des améliorations technologiques (efficacité énergétique des data
centers notamment) doivent être mises en œuvre.

La sobriété dans les usages du numérique doit être promue, de
même que l’allongement de la durée de vie des ordinateurs, la
réparation des appareils ou leplafonnement dutaux d’équipement
par personne.

En termes de gouvernance, la construction de nouveaux réseaux et
l’acquisition de nouveaux appareils doivent prendre en compte
l’impact carbone qu’elles génèrent et être compatibles avec la
stratégie environnementale du territoire

Les questions qui nous restent à explorer
-------------------------------------------

Dans  le  secteur
    L’évolution du nombre d’emplois n’a pas encore été  chiffrée, et
    l’impact écologique du numérique doit encore être chiffré
    dans le cadre du territoire français.

En  lien  avec  le  reste  de  l’économie
    La  mise  en  cohérence  avec  les  secteurs impactés par la
    transformation numérique doit encore être affinée

L’emploi
-----------

La transformation proposée par le Plan de transformation devrait remettre
en question des créations d’emplois envisagées dans des projections
connectées de notre économie, mais un chiffrage précis n’est pas encore
disponible


Les impacts
------------

Les plus

    Le secteur du numérique a réduit ses émissions de GES, est plus
    résilient et permet d’accompagner la transformation globale de la société.

    Il reste un outil majeur de communication et d’efficience économique.

Les limites
    La limitation des usages individuels du numérique est complexe à
    mettre en œuvre car elle nécessite un changement profond des mentalités
    et modes  de consommation.





