.. index::
   pair: sobriété numérique; Ressources

.. _ressources_sobri_latech:

=====================================
Ressources sobriété numérique
=====================================

.. toctree::
   :maxdepth: 3

   negaoctet/negaoctet
   numeriqueEcoResponsable/numeriqueEcoResponsable
   theshiftproject/theshiftproject



TODO
=====

https://negawatt.org/
-------------------------

https://negaoctet.org/
-----------------------

https://www.sauvonsleclimat.org/fr/
--------------------------------------
