.. index::
   pair: Campagnes ; 2020-10-30
   pair: Chantier; Infra et logistique interne
   pair: Chantier; Identité et valeurs du mouvement
   pair: Chantier; Lobbying et positionnements publics
   pair: Chantier; Éducation populaire et vulgarisation
   pair: Chantier; Vision de la tech et éthique en entreprise
   pair: Chantier; Création d'un label "onestlatech"
   ! Chantiers

.. _campagnes_2020_10_30:

=====================================
4.Thématiques et campagnes à mener
=====================================

.. seealso::

   - https://mensuel.framapad.org/p/onestlatech-9jt6?lang=fr
   - https://onestla.tech/publications/une-autre-tech-est-possible/


.. contents::
   :depth: 3


Idées de groupes de travail
===============================

- Le nom : on le garde, ou pas ?
- Les syndicats : on en fait un ? On essaye de fédérer les existants ?
  On ne touche pas parce que c'est pas notre volonté ?
- Quels outils pour communiquer ? (Mattermost, RocketChat, Jabber, Mumble,
  Jitsi, Matrix/Riot etc.)
- Quels moyens pour héberger ces outils ?
- **Rédaction d'un manifeste commun ?**
- Richard : selon moi, OnestlaTech doit etre fondamentalement technocritique,
  ni technophile beat, ni technophobe, l'entre deux quoi
- **éthique au sein de l'entreprise** (voir ceux complets à pomper : reset, etc...)
- logiciel libre
- **Shaming** : noter les entreprises en fonction des valeurs du collectif
  (logiciels libres, tracking, salaires, diversité...)
- Comment orienter nos métiers ? (temps de travail, arrêter de bosser
  pour le capitalisme, pour qui travailler, travailler avec les assos
  et think tank, etc.)
- création d'un label "onestlatech" ?
- s'incruster dans les conférences pour faire entendre nos voix,
  s'exprimer sur ces sujets
- **Education populaire** (divers publics, personnes jeunes, personnes âgées,
  personne peu à l'aise avec le numérique...)
- fracture numérique (à grouper avec éducation populaire ?) + vulgarisation
  grand public (exemple des RS et des données collectées à expliquer à
  nos proches, ce genre de choses)
- grands enjeux de société (5G etc)
- éthique en entreprise (comment refuser certains clients cf l'exemple
  de ICE aux USA, refuser d'implémenter certaines pratiques comme le
  tracking etc)
- responsabiliser les utilisateurs, responsabiliser les producteurs
- militer pour un droit de retrait tech lié à notre éthique (objection
  de conscience)
- qui est notre cible ? Pros de la tech / grand public / les deux ?
- faire du lobbying par exemple pour que l'Etat utilise forcément et
  donc investisse dans le libre. Public Money = Public Code (campagne FSF).
  Pour un service public du  logiciel libre et des communs ! #lobbying
- Numérique responsable : accessibilité, sobriété, illectronisme #sobriete++
- ? (organisation du militantisme et des manifestations à venir)
- Maintenance du site

Proposition de structuration de ces chantiers (par Mupsi)
==========================================================

.. _chantier1_2020_10_30:

[chantier 1] Gouvernance et structure interne
-----------------------------------------------

.. seealso::

   - :ref:`chantier_1_latech`


- Le nom : on le garde ou pas ?
- Forme juridique : syndicat, asso loi 1901, pas de structure...?
- Les syndicats : on en fait un ? On essaye de fédérer les existants ?
  On ne touche pas parce que c'est pas notre volonté ?
- Quelle forme de gouvernance ?
- Fonctionnement décentralisé / asynchrone

.. _chantier2_2020_10_30:

[chantier 2] Infra et logistique interne
-------------------------------------------

.. seealso::

   - :ref:`naywel_outils_2020_10_30`
   - :ref:`chantier_2_latech`


- :ref:`Quels outils pour communiquer <naywel_outils_2020_10_30>`?
  (Mattermost, RocketChat, Jabber, Mumble, Jitsi, Matrix/Riot...)
- Quels moyens pour héberger ces outils ?
- Organisation du militantisme et des manifestations à venir : rendez-vous récurrents
- Maintenance du site et des divers outils
- Stratégie de communication sur les réseaux sociaux

.. _chantier3_2020_10_30:

[chantier 3] Identité et valeurs du mouvement
-----------------------------------------------

.. seealso::

   - :ref:`idee_manfeste_2020_10_30`
   - https://reset.fing.org/participer-a-reset/le-dispositif.html
   - :ref:`chantier_3_latech`


- Valeurs : "de gauche", diversité, inclusion...?
- **Rédaction d'un manifeste commun ?**
- Positionnement : technocritique (ni technophile beat, ni technophobe),
  etc ?
- Qui est notre cible : pros de la tech / grand public / les deux ?

.. _chantier4_2020_10_30:

[chantier 4] Lobbying et positionnements publics
-----------------------------------------------------

- Se positionner sur de grands enjeux de société (5G etc)
- S'incruster dans les conférences pour faire entendre nos voix,
  s'exprimer sur ces sujets
- Militer pour que l'Etat utilise forcément et donc investisse dans le
  libre = pour un service public du  logiciel libre et des communs
- Militer pour un droit de retrait tech lié à notre éthique (objection
  de conscience)
- **Shaming** : noter les entreprises en fonction des valeurs du collectif
  (logiciels libres, tracking, salaires, diversité...))

.. _chantier5_2020_10_30:

[chantier 5] Éducation populaire et vulgarisation
--------------------------------------------------

.. seealso::

   - :ref:`chantier_5_latech`

- Divers publics, personnes jeunes, personnes âgées, personne peu à
  l'aise avec le numérique...
- Fracture numérique
- Vulgarisation grand public (exemple des RS et des données collectées
  à expliquer à nos proches, ce genre de choses)

.. _chantier6_2020_10_30:

[chantier 6] Vision de la tech et éthique en entreprise
--------------------------------------------------------

.. seealso::

   - :ref:`chantier_6_latech`

- Accompagnement : comment refuser certains clients (cf l'exemple de ICE
  aux USA, refuser d'implémenter certaines pratiques comme le tracking etc)
- Sujet des lanceurs et lanceuses d'alerte
- Numérique responsable : accessibilité, sobriété, illectronisme
- Responsabiliser les utilisateurs, responsabiliser les producteurs
- Comment orienter nos métiers : temps de travail, arrêter de bosser
  pour le capitalisme, pour qui travailler, travailler avec les assos
  et think tank, etc.

[chantier 7] Création d'un label "onestlatech"
---------------------------------------------------

