.. index::
   ! Chantier 3
   pair: Chantier; Identité et valeurs du mouvement

.. _chantier_3_latech:

============================================================
Chantier 3 **Identité et valeurs du mouvement**
============================================================

.. contents::
   :depth: 3


Origine
=========

.. seealso::

   - :ref:`chantier3_2020_10_30`
   - https://mensuel.framapad.org/p/onestlatech-9jt6?lang=fr


Description
=============

.. seealso::

   - :ref:`naywel_outils_2020_10_30`


- Valeurs : "de gauche", diversité, inclusion...?
- **Rédaction d'un manifeste commun ?**
- Positionnement : technocritique (ni technophile beat, ni technophobe),
  etc ?
- Qui est notre cible : pros de la tech / grand public / les deux ?

Code de conduite
================

.. toctree::
   :maxdepth: 3

   code_de_conduite/code_de_conduite.rst


Ressources
===========

.. toctree::
   :maxdepth: 3

   ressources/ressources
