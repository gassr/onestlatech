.. index::
   pair: RFCs ; Identité et valeurs du mouvement

.. _chantier_3_rfcs:

======================================================
RFCs
======================================================

.. contents::
   :depth: 3


RFC 8280 (Research into Human Rights Protocol Considerations, 2017-11)
=========================================================================

.. seealso::

   - https://www.bortzmeyer.org/8280.html



RFC 8890: The Internet is for End Users, 2020-08
====================================================

.. seealso::

   - https://www.bortzmeyer.org/8890.html
