.. index::
   pair: Framapad ; Identité et valeurs du mouvement

.. _framapad_chantier_3_latech:

======================================================
Framapad
======================================================

.. contents::
   :depth: 3


Framapad
=========

.. seealso::

   - https://annuel2.framapad.org/p/valeurs---onestlatech-9jzs?lang=fr


I - Lutter contre les inégalités dans la répartition des richesses
=======================================================================

- automatisation
- accaparement des richesses par une minorité
- lutte contre la pauvreté
- défense des acquis sociaux (fonctionnaires / chômage)
- défense du service public
- diversité (sexisme, racisme, anti-LGBT)
- soutien des luttes sociales


II - Lutter contre le productivisme / Lutter contre le capitalisme sauvage
================================================================================

- consumérisme
- abolition du capitalisme
- libération du temps libre
- impacts environnementaux
- amélioration des conditions de travail
- Uberisation
- lutte contre le réchauffement climatique
- défense de l'Université
