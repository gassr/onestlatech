.. index::
   ! Code de conduite

.. _code_de_conduite:

======================================================
Code de conduite
======================================================


.. contents::
   :depth: 3


Framapad
=========

.. seealso::

   - https://semestriel.framapad.org/p/code-de-conduite-----onestlatech-9k08?lang=en


Notre promesse
==================

Nous, membres du collectif onestla.tech/, promettons de faire de notre
communauté des espaces d'échanges où le harcèlement et l'hostilité
n'ont pas leur place.

Toute personne, quel que soit son âge, sa race,  son identité de genre,
son orientation sexuelle, son niveau d'éducation, son apparence physique,
sa classe sociale, sa nationalité, ses handicaps (visibles ou non),
son niveau d'expérience, sa religion ou tout autre aspect de son
identité, doit trouver dans notre communauté un espace où elle puisse
échanger et participer aux débats sans être jugée ou attaquée.

En tant que membres du collectif onestla.tech, nous nous engageons à
entretenir des espaces de paroles libres, respectueux, accueillant,
inclusifs et bienveillants.

A ces fins, toute personne qui rejoint notre collectif adhère de fait à
ce code de conduite.


Nos standards
=================

Afin de créer un environnement positif, voici quelques exemples de comportements qui sont encouragés :


- Utiliser un langage inclusif. (Attention par exemple à ne pas toujours
  tout genrer au masculin).
- Respecter les divers points de vue, expériences et opinions.
- Donner un feedback constructif et bienveillant et accueillir sereinement
  ceux qu'on nous adresse.
- Se remettre en question et s'excuser lorsque le besoin s'en fait ressentir.
- Accueillir chaleureusement les nouveaux arrivants et faire en sorte
  qu'ils s'intègrent facilement au groupe.
- D'une manière générale, traiter chaque échange avec empathie et bienveillance.


A l'inverse, sont considérés comme des comportements inacceptables :

- L'utilisation de langage ou d'imagerie sexualisé/sexualisant ainsi que toute attention sexuelle ou avance non consentie.
- Les insultes, le trolling, les commentaires péjoratifs, les attaques personnelles.
- Le harcèlement, qu'il soit privé ou publique.
- La publication d'informations privées d'une personne sans son consentement.
- Tout autre comportement qui ne serait pas toléré dans un cadre professionnel.



La portée de ce code
============================

Ce code de conduite s'applique à tous les espaces communautaires du
collectif onestla.tech/, qu'ils soient virtuels, publiques (les channels
du discord, etc...), privés (les échanges entre deux membres par messages
privés) ou IRL (les réunions en présentiel, etc...).

Il s'applique aussi tout particulièrement aux discours et écrits des
personnes chargées de représenter le collectif face au publique (mandats, etc...).


L'application du code :

A DEFINIR



Sources :

Ce code a utilisé comme principal modèle/source d'inspiration le "Contributor covenant V2.0" (https://www.contributor-covenant.org/) ainsi que l'article sur les codes de conduite de "Open Source Guides" (https://opensource.guide/fr/code-of-conduct/).

