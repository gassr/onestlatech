.. index::
   ! Chantier 1
   pair: Chantier; Gouvernance et structure interne

.. _chantier_1_latech:

================================================
Chantier 1 **Gouvernance et structure interne**
================================================

.. seealso::

   - https://discordapp.com/channels/654441797539594277/772799443434340373/772950941028057108
   - https://hebdo.framapad.org/p/R%C3%A9capitulatif_des_diff%C3%A9rences_structurelles-9jv3?lang=fr
   - https://annuel2.framapad.org/p/onestlatech-syndicat-9jt7?lang=fr

.. contents::
   :depth: 3


Origine
=========

.. seealso::

   - :ref:`chantier1_2020_10_30`
   - https://mensuel.framapad.org/p/onestlatech-9jt6?lang=fr


Description
=============

- Le nom : on le garde ou pas ?
- Forme juridique : syndicat, asso loi 1901, pas de structure...?
- Les syndicats : on en fait un ? On essaye de fédérer les existants ?
  On ne touche pas parce que c'est pas notre volonté ?
- Quelle forme de gouvernance ?
- Fonctionnement décentralisé / asynchrone


Ressources
==============

.. toctree::
   :maxdepth: 3

   ressources/ressources

