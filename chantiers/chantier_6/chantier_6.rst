.. index::
   ! Chantier 6
   pair: Chantier; Vision de la tech et éthique en entreprise

.. _chantier_6_latech:

========================================================================
Chantier 6 **Vision de la tech et éthique en (entreprise seulement ?)**
========================================================================



.. contents::
   :depth: 3


Origine
=========

.. seealso::

   - :ref:`chantier6_2020_10_30`
   - https://mensuel.framapad.org/p/onestlatech-9jt6?lang=fr


Page framapad
==============

.. seealso::

   - https://annuel2.framapad.org/p/ew4lb5grz7-9jzm?lang=en


Description
=============

- Accompagnement : comment refuser certains clients (cf l'exemple de ICE
  aux USA, refuser d'implémenter certaines pratiques comme le tracking etc)
- Sujet des **lanceurs et lanceuses d'alerte**
- **Numérique responsable : accessibilité, sobriété, illectronisme**
- Responsabiliser les utilisateurs, responsabiliser les producteurs
- Comment orienter nos métiers : temps de travail, arrêter de bosser
  pour le capitalisme, pour qui travailler, travailler avec les assos
  et think tank, etc.


Posts
======

.. toctree::
   :maxdepth: 5

   posts/posts


Ressources
============

.. toctree::
   :maxdepth: 5

   ressources/ressources


TODO
====

charles 2020-11-04
------------------------------------------

- https://discordapp.com/channels/654441797539594277/772804634334068736/773609307722743848

ça serait bien de faire une petite bibliographie de textes classiques et
de trucs plus récents (mélangeant manifestes et vrais bouquins) qui
traite de la technologie et dans lesquels ont peut se reconnaître
(ou pas - et dans ce cas la pointer vers les critiques). Vous en pensez quoi ?


https://framablog.org/2010/05/22/code-is-law-lessig/
---------------------------------------------------------

The Hippocratic License 2.1 : https://firstdonoharm.dev/
----------------------------------------------------------

- https://framablog.org/2017/11/17/le-chemin-vers-une-informatique-ethique/
- https://framablog.org/2017/09/19/createurs-du-numerique-parlons-un-peu-ethique/
- https://blog.imirhil.fr/2017/02/21/logiciel-libre-gouvernance-ethique.html

https://www.contributor-covenant.org/
----------------------------------------

A Code of Conduct for Open Source Projects : https://www.contributor-covenant.org/ par https://where.coraline.codes/

Ethical Source: Open Source, Evolved : https://ethicalsource.dev/
------------------------------------------------------------------

https://agilemanifesto.org/iso/fr/manifesto.html
-------------------------------------------------

Vocabulaire/glossaire
======================

- :term:`techno-discernement`
