.. index::
   ! Chantier 2
   pair: Chantier; Infra et logistique interne

.. _chantier_2_latech:

============================================================
Chantier 2 **Infra et logistique interne**
============================================================

.. contents::
   :depth: 3


Origine
=========

.. seealso::

   - :ref:`chantier2_2020_10_30`
   - https://mensuel.framapad.org/p/onestlatech-9jt6?lang=fr
   - https://ag.onestla.tech/explore?order=memberships_count

Description
=============

.. seealso::

   - :ref:`naywel_outils_2020_10_30`

- :ref:`Quels outils pour communiquer <naywel_outils_2020_10_30>`?
  (Mattermost, RocketChat, Jabber, Mumble, Jitsi, Matrix/Riot...)
- Quels moyens pour héberger ces outils ?
- Organisation du militantisme et des manifestations à venir:
  rendez-vous récurrents
- Maintenance du site et des divers outils
- Stratégie de communication sur les réseaux sociaux

Outils
=========

.. toctree::
   :maxdepth: 6

   outils/outils
