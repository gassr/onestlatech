.. index::
   ! padlet

.. _padlet:

====================================================================
padlet (**ce n'est pas un logiciel libre**, centralisé, pour info)
====================================================================

.. seealso::

   - https://twitter.com/padlet
   - https://padlet.com/
   - https://jn.padlet.com/

.. contents::
   :depth: 3

Caractéristiques
==================

- ce n'est pas un logiciel libre
- centralisé
- pour la petite histoire, utilisé par la copropriété
