.. index::
   pair: Définition ; syndicat


.. _def_syndicat_1:

===============================
C’est quoi un Syndicat ?
===============================

.. seealso::

   - https://annuel2.framapad.org/p/onestlatech-syndicat-9jt7?lang=fr

.. contents::
   :depth: 3


Intervention de Nas (CNT)
===============================

.. figure:: cnt_nas.png
   :align: center


@dunglas On est toujours preneurs de critiques à la CNT, par contre fait
les quand on est là.

On est pas un musée, on redéfinit en permanence l’anarchosyndicalisme et
le syndicalisme révolutionnaire : par exemple on a adopté d’écriture
inclusive dès 96, l’écologie dès 94, et bien sur qu’on est anti-productiviste.

On a même décidé dans la même période qu’il était possible de participer
aux élections pro sous conditions. Bref renseigne toi, ou arrange toi
pour être avec des gens qui savent.

Et je suis désolé d’avoir à y répondre, parce que là le sujet c’est de
vulgariser, pas de faire des débats techniques(modifié)


pour ce qui est de la fiche récàp avec une tentative de définition, j’en
ai fait une : https://annuel2.framapad.org/p/onestlatech-syndicat-9jt7?lang=fr

N’hésitez pas à amender, ou supprimer des passages. C’est fait pour


Vis à vis d’une association ou d’une coopérative
=================================================

.. seealso::

   - https://annuel2.framapad.org/p/onestlatech-syndicat-9jt7?lang=fr


En Allemagne, un syndicat est une association ayant pour objet la défense
des travailleuses et des travailleurs.

Il n’y a donc, à priori, pas de différences avec une association.

Juridiquement, en France un syndicat dispose de plusieurs capacités
particulières : celles de représenter juridiquement les salariés, est
proche aux associations de défenses d’intérêts.

Cependant, ce statut garantit aussi un certaine anonymat, y compris
vis-à-vis de la police, et de libertés d’expression.
C’est lié à son rôle historique de lanceurs d’alertes.

Si l’adhésion a un syndicat peut-être pseudyme, la reconnaissance par un
syndicat d’un action ou d’une parole permet de protéger un·e salarié·e·s
dans le cadre de ses revendications ou de son expression.

La différence avec une association, en plus des protections des données,
et simplement la plus grande facilité a se faire reconnaître comme
compétent sur le lieu de travail.
À l’inverse, cela ne discrédite pas pour autant un syndicat en dehors
de celui-ci.

Pour les syndicats qui sont d’accord pour participer, il est possible
d’être élus en tant que salariés, et d’avoir accès à des informations
protégées, comme les dépenses de l’entreprise.
Ces prérogatives changent en fonction du type de structure.
Les universités par exemple doivent soumettre aux votes des représentants
syndicaux certaines décisions.

Un syndicat permet aussi de gérer des formations syndicales.

Les salarié·e·s —syndiqué·e·s ou non— peuvent suivre sur leur temps de
travail des formations proposées par un syndicat.

Dans le privé, il y 12 jours de formations par an, 14 pour les elu·e·s.

L’employeur n’a pas le droit de refuser, ni à connaitre le contenu de la
formation, et læ salarié·e est payé normalement pendant cette formation.

Dans la définition, un syndicat reste une association de défense des
intérêt de ses membres (copro, patronat, etc…).

Beaucoup d’association sont en fait des syndicats, mais n’en ont simplement
pas le statut, où ne s’en réclament pas.

Du coup un syndicat n’a pas d’autres champs actions propres que ses statuts.

D’ailleurs le premier syndicat national ( la fédération des bourses du travail )
avait autant le rôle de mettre en relation travailleu·r·ses et patrons,
de former le prolétariat, que d’aider à la constitution de coopératives,
qui sont nées de là.

Ça avait plusieurs avantages : fixer les salaires et les conditions de
travail collectivement, permettre une transmission des savoir-faires
parmis celles et ceux qui defendent les mêmes intérêts, et faire en sorte
que les coopératives soient une alternative au salariat, et non une concurrence.


Des définitions différentes
===============================

Historiquement
---------------

Si historiquement les syndicats, très proches des anarchistes, sont créés
dans un objectif de changer le système productifs dans l’intérêt de toutes
et tous, en évitant de se faire récupérer par les partis politiques.

Ce que explique le fonctionnement dont ils héritent :  des syndicats par
la base, fédérés par savoir-faire, et confédérés sur un territoire.

C’est évidemment ironique vu leur développement futur: le parti
communiste, puis socialiste, qui à force de noyautage ont détourné
ces organisations.
Aujourd’hui, les centrales syndicales reflètent ces visions particulières :
un centralisme des négociations, voire de la cogestion.

De transformation sociale
--------------------------

Les premiers syndicats ont pour objectif de changer radicalement la société
depuis son système productif.
Il s’agit de faire la révolution via la grève et l’autogestion des entreprises,
à la place de l’insurection, suite à l’expérience de la commune.
Elle sera illustrée en Espagne avec l’expérience libertaire de 1936 en
Catalogne et en Aragon.

Dans la seconde moitié du XXe siècle la CGT impose une vision plus
autoritaire de cette transformation sociale.
Si elle fait toujours partie des syndicats de luttes, ça la différencie
des syndicats par démocratie direct (SUD SOLIDAIRE) ou autogéré (CNT).

Ce positionnement explique pourquoi les syndicats s’expriment sur le
fascisme, l’écologie, l’exil, et d’autres sujets de sociétés.
Si à priori la charte d’Amiens interdit aux syndicats de se mêler de
politique, il s’agit en fait d’une volonté de se distancer des partis
politiques.

De cogestion
--------------

D’autres syndicats veulent réformer le capitalisme.

Il ne s’agit alors que de mutualiser les services (juridique, de formation)
aux salarié·e·s. Généralement, ils abandonnent leur rôle de lanceurs
d’alertes, puisqu’ils cherchent à travailler avec les dirigeants, et non
avec le reste de la société. C’est le cas de la CFDT.

Si historiquement, ce sont souvent des syndicats crées par les patrons
pour contrer les syndicats de luttes, les choses sont aujourd’hui plus
complexes, avec des syndiquées à la base qui sont souvent plus revendicatifs
que leur centrale, au point d’être trahit par elle.
On peut citer la CFDT qui a dissout et volé l’argent d’un de ses syndicat
qui ne voulait pas signer un accord avec son employeur.

Est ce qu’au final on s'en balance non ?
===========================================

La forme juridique du syndicat est un outil qui est objectivement
meilleur sur ce plan.
Par contre les centrales syndicales ont du mal avec la diversité.
Il faut donc imposer un rapport de force avec elle. Elles ont déjà
sabotées des initiatives similaires.

On peut citer des associations de luttes qui gagnent des luttes
(Droit au Logement, Dissenssus, La Quadra).
En général ce qui est déterminant c’est le rapport de force.

À savoir sur les syndicats
============================

Bien que peu de confédérations soient encore dirigées par la base, elles
en hérite la forme confédérales : des syndicats à la base sont autonomes
sur une grande partie de leurs activités.

Ils se fédérent avec les syndicats partageant le même savoir-faire
(généralement des industries ou des métiers), et s’unissent sur leur
zone géographique.

On a donc:

- Syndicat
- Union Local (généralement les syndicats d’une ville)
- Union Départementale
- Union Régionale
- Fédération de l’industrie ou du métier du syndicat

Il y a des syndicats de métallurgie, d’éducation, mais à défaut il peut
exister des syndicats interprofessionnels, qui permettent une activité
pour les personnes n’étant pas assez nombreuses pour former un syndicat.

Car s’il faut être 2 pour juridiquement former un syndicat, ça peut-être
insuffisant pour être visible (rédiger des tracts, former des cortèges,
tenir des permances syndicales, …).

Les syndicats tiennent souvent des permanences, afin de conseiller et
aider les salarié·e·s et de permettre leur adhésion.

Il n’échappera à personne la culture Marxienne, voir marxiste des
syndicats de luttes. Elles impliquent une solidarité entre les
travailleuses et travailleurs.
Ça implique qu’à la base, les syndicats de lutte peuvent vous aider
si vous êtes manifestement dans une démarche de lutte des classes.

Des Confédérations Syndicales travaillent régulièrement ensemble,
comme pour organiser le 1er Mai, tout comme leur fédérations.

Par exemple lorsque les personnels soignants appellent à la grève, ielles
le font au travers d’une réunion des représentant·e·s locaux des
fédérations des personnels soignants. Dans le premier cas, comme le second,
on appelle ça l’intersyndicale.

Critique de la forme syndicale
===================================

Un syndicat est une organisation.

Il y a donc des luttes et des abus de pouvoirs.

Beaucoup de critiques des syndicats pointent les trahisons de leurs
bureau parisien, et pourraient être étendue à des associations de
luttes ou des partis.

On reproche aussi le productivisme aux syndicats, si c’était vrai des
tendances majoritaires, ça l’est de moins en moins avec la montée du
syndicalisme vert dans les confédérations.

S’organiser sur la question du travail n’empêche pas sa critique, au contraire.








