
.. _onestlatech:
.. _ref_latech:

====================================================================
OnEstLaTech
====================================================================

.. figure:: _static/1500x500.webp
   :align: center


- https://discord.gg/se3PnEr
- https://twitter.com/OnEstLaTech
- https://mastodon.social/@onestlatech
- https://onestla.tech/
- https://ag.onestla.tech/explore?order=memberships_count


.. toctree::
   :maxdepth: 5

   manifeste/manifeste
   ethique/ethique
   reunions/reunions
   chantiers/chantiers

.. toctree::
   :maxdepth: 6

   ressources/ressources

.. toctree::
   :maxdepth: 5

   syndicats/syndicats
   glossaire/glossaire
   meta/meta
